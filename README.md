## Prerequisites

- Rust programming language and Cargo package manager installed.
- AWS CLI configured with appropriate AWS account credentials.
- [Optional] Docker for testing Lambda functions locally.

## Setup and Installation

### 2. Install Dependencies

Ensure your `Cargo.toml` has the necessary dependencies for AWS SDK for Rust (or Rusoto if you're using it), Lambda runtime, and any other libraries you're using.

```toml
[dependencies]
# Example dependencies
aws-sdk-dynamodb = "0.13.0"
lambda_runtime = "0.4.1"
tokio = { version = "1", features = ["full"] }
```

Run `cargo build` to download and compile dependencies.

### 3. Environment Configuration

Set up environment variables for local development, if necessary. For example, to specify the AWS region and DynamoDB table name:

```bash
export AWS_REGION='us-east-1'
export DYNAMODB_TABLE='yourTableName'
```

### 4. Write Your Lambda Function

Implement your Lambda function logic in Rust. Use the AWS SDK for Rust to interact with DynamoDB within your function.

### 5. Build Your Lambda Function

Compile your Lambda function targeting the AWS Lambda Linux runtime.

```bash
cargo lambda build --release --target x86_64-unknown-linux-musl
```

### 6. Deploy Your Lambda Function

Deploy your function to AWS Lambda using the AWS CLI or the `cargo lambda` tool.

```bash
cargo lambda deploy --iam-role arn:aws:iam::ID:role/YourLambdaExecutionRole
```

# AWS Lambda DynamoDB Integration

- The Lambda function is designed to receive HTTP requests and the data is stored in an Amazon DynamoDB table.


- The Lambda function is triggered via an API Gateway. It expects to receive a `name` parameter through the query string, which it then stores in a DynamoDB table named `table1`.

## DynamoDB Table

The DynamoDB table has the following configuration:

- **Table name**: `table1`
- **Partition key**: `a` (String)

Ensure that the AWS region for the DynamoDB table matches the region you intend to deploy your Lambda function to.

## Lambda Function

The Lambda function is written in Rust and utilizes the `rusoto_dynamodb` crate to interact with DynamoDB.

Upon invocation via the API Gateway, the Lambda function executes the following steps:

1. It parses the incoming HTTP request for a `name` query parameter.
2. It constructs an item with this `name` and uses `DynamoDbClient` to insert it into `table1`.

The function utilizes `lazy_static` to initialize the `DynamoDbClient` to improve performance by reusing the same client across invocations.

- After deploying, you can invoke the Lambda function by navigating to the provided API Gateway URL:

https://<api_id>.execute-api.<region>.amazonaws.com/<stage>/lambda-function?name=<value>

markdown
Copy code

Replace `<api_id>`, `<region>`, `<stage>`, and `<value>` with your actual API Gateway ID, AWS region, deployment stage, and the name you wish to store, respectively.


## Deployment

Additional notes about how to deploy this on a live system.

## API Gateway Integration

1. Create an API Gateway
2. Add Lambda Integration
3. Configure Routes
4. Define Stages
5. Deploy and Test

```sh
curl "https://your_api_id.execute-api.region.amazonaws.com/stage/resource_path?query_parameters"
```

## Screen shots:
- Function
    - ![Alt text](./images/image0.jpeg "Optional title0")

- Deploy it on the AWS
    - ![Alt text](./images/image1.jpeg "Optional title1")
    - ![Alt text](./images/image2.jpg "Optional title2")


- API Gateway Integration
    - ![Alt text](./images/image11.jpeg "Optional title4")
    - ![Alt text](./images/image12.jpeg "Optional title5")


- Add logging to the Rust Lambda funciton
    - ![Alt text](./images/image4.jpeg "Optional title6")

- Integrate AWS X-Ray tracing
    - ![Alt text](./images/image5.jpeg "Optional title7")

- CloudWatch Logs
    - ![Alt text](./images/image6.jpg "Optional title8")

- Traces
    - ![Alt text](./images/image7.jpg "Optional title9")