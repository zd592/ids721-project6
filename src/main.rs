use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};
use lazy_static::lazy_static;

extern crate lazy_static;

use rusoto_core::Region;
// use rusoto_dynamodb::DynamoDbClient;
use rusoto_dynamodb::{DynamoDb, DynamoDbClient, PutItemInput, AttributeValue};

lazy_static! {
    static ref DDB_CLIENT: DynamoDbClient = DynamoDbClient::new(Region::UsEast1);
}


/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Log the reception of a request
    tracing::debug!("Received a request.");

    // Extract some useful debugrmation from the request
    let who = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("name"))
        .unwrap_or("world");

    // Log the extracted debugrmation
    tracing::debug!("Request for name: {}", who);

    let table_name = "table1".to_string(); // Make sure this matches your actual DynamoDB table name
    let item = {
        let mut item = std::collections::HashMap::new();
        item.insert("a".to_string(), AttributeValue { // "a" should match the Partition Key of your DynamoDB table
            s: Some(who.to_string()),
            ..Default::default()
         });
        item
    };

    let put_request = PutItemInput {
        table_name,
        item,
        ..Default::default()
    };

    match DDB_CLIENT.put_item(put_request).await {
        Ok(_) => tracing::debug!("Item inserted successfully."),
        Err(e) => tracing::error!("Error inserting item: {:?}", e),
    }
    

    let message = format!("Hello {who}, this is an AWS Lambda HTTP request");

    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(message.into())
        // .map_err(Box::new)?;
        .map_err(|e| {
            // Log the error before converting it to the Error type
            tracing::error!("Failed to create response: {}", e);
            Box::new(e) as Box<dyn std::error::Error + Send + Sync>
        });

    // Ok(resp)

    // Log successful response creation
    match &resp {
        Ok(_) => tracing::debug!("Successfully created response."),
        Err(e) => tracing::error!("Error creating response: {:?}", e),
    }

    resp
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::DEBUG.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
